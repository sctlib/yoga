const LinkExternal = ({
	children,
	href,
	className
}) => {
	return (
		<a
			href={href}
			target="_blank"
			rel="noreferer noopener"
			className={`LinkExternal ${className ? className : ''}`}>
			{children}
		</a>
	)
}

export default LinkExternal
