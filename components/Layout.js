import Link from "next/link";

const Layout = ({ children }) => {
	return (
		<div className="Site">
			<header className="Site-header">
				<nav className="Nav">
					<Link href="/">
						<span className="Nav-item">
							<h1>Yoga</h1>
						</span>
					</Link>
					<Link href="/postures">
						<span className="Nav-item">Postures</span>
					</Link>
				</nav>
			</header>
			<main className="Site-main">{children}</main>
		</div>
	);
};

export default Layout;
