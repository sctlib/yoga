---
name_asana: "Sarvangasana"
name_international: "Shoulder stand"
name_sanskrit: " 	सालम्बसर्वाङ्गासन"
asana_type: "Inversion"
url_wikipedia: "https://en.wikipedia.org/wiki/Sarvangasana"
---

Sarvangasana, Shoulderstand, or more fully Salamba Sarvangasana (Supported Shoulderstand), is an inverted asana in modern yoga as exercise; similar poses were used in medieval hatha yoga.

Many named variations exist, including with legs in lotus position and Supta Konasana with legs wide apart, toes on the ground. 
